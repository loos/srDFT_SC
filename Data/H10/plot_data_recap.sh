basis=dz
atom=N

 FCI=-108.9739012
 FCI_LDA=-109.02698261
 FCI_PBE=-109.02004929
 FCI_PBEont=-109.02337969
 FCI_PBEontden=-109.02120186

 tFCI=-109.03390852
 tFCI_LDA=-109.05776664519999
 tFCI_PBE=-109.0534328248
 tFCI_PBEont=-109.05786915259999
 tFCI_PBEontden=-109.057425162

 exact=-109.1698


cat <<EOF > plot.plt

set yrange [:0.1]
plot "data_${atom}2_av${basis}_cas66" u 1:(\$3 - $FCI) w l lw 3 title "FCI dz", "" u 1:(\$3+\$9 -$FCI_PBEontden ) w l lw 3 title "FCI dz +PBE on top / density" 
replot "data_${atom}2_avtz_cas66" u 1:(\$3 - $tFCI) w l lw 3 title "FCI tz", "" u 1:(\$3+\$9 -$tFCI_PBEontden ) w l lw 3 title "FCI+PBE tz on top / density" , 'exact-${atom}2' u (\$1 * 0.529177):(\$2 - $exact) w l lw 3 smooth csplines title "Exact spline", 'exact-${atom}2' u (\$1 * 0.529177):(\$2 - $exact) w p ps 0.5  notitle 
set term eps 
set output "${atom}2_tz_PBE_on_top_den_recap.eps"
replot

EOF

gnuplot plot.plt
