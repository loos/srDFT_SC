#!/bin/bash

basis=DZ
atom=N

cat <<EOF > plot_truncated.plt
#plot "data_${atom}2_avdz_cas66_truncated" u 1:2 w l lw 3 title "CAS", -108.7797414 w l lw 3 title "2*${atom} CAS", "" u 1:(\$2+\$3) w l lw 3 title "CAS+LDA", -108.91410101 w l lw 3 title "2*(CAS ${atom} + LDA)", "data_${atom}2_avdz_cas66" u 1:3 w l lw 3 title "FCI", -108.9739012 w l lw 3 title "2*${atom} FCI", "" u 1:(\$3+\$6) w l lw 3 title "FCI+LDA", -109.02698261 w l lw 3 title "2*(FCI ${atom} + LDA)"
plot "data_${atom}2_avdz_cas66_truncated" u 1:2 w l lw 3 title "CAS", "" u 1:(\$2+\$3) w l lw 3 title "CAS+LDA", "data_${atom}2_avdz_cas66" u 1:3 w l lw 3 title "FCI", "" u 1:(\$3+\$6) w l lw 3 title "FCI+LDA"
set term eps 
set output "${atom}2_${basis}_LDA_tot.eps"
replot

#plot "data_${atom}2_avdz_cas66_truncated" u 1:2 w l lw 3 title "CAS", -108.7797414 w l lw 3 title "2*${atom} CAS", "" u 1:(\$2+\$4) w l lw 3 title "CAS+PBE", -108.92104742 w l lw 3 title "2*(CAS ${atom} + PBE)", "data_${atom}2_avdz_cas66" u 1:3 w l lw 3 title "FCI", -108.9739012 w l lw 3 title "2*${atom} FCI", "" u 1:(\$3+\$7) w l lw 3 title "FCI+PBE", -109.02004929 w l lw 3 title "2*(FCI ${atom} + PBE)"
plot "data_${atom}2_avdz_cas66_truncated" u 1:2 w l lw 3 title "CAS", "" u 1:(\$2+\$4) w l lw 3 title "CAS+PBE",  "data_${atom}2_avdz_cas66" u 1:3 w l lw 3 title "FCI", "" u 1:(\$3+\$7) w l lw 3 title "FCI+PBE"
set term eps 
set output "${atom}2_${basis}_PBE_tot.eps"
replot

#plot "data_${atom}2_avdz_cas66_truncated" u 1:2 w l lw 3 title "CAS", -108.7797414 w l lw 3 title "2*${atom} CAS", "" u 1:(\$2+\$5) w l lw 3 title "CAS+PBE on top", -108.90744369 w l lw 3 title "2*(CAS ${atom} + PBE on top)", "data_${atom}2_avdz_cas66" u 1:3 w l lw 3 title "FCI", -108.9739012 w l lw 3 title "2*${atom} FCI", "" u 1:(\$3+\$8) w l lw 3 title "FCI+PBE on top", -109.02337969 w l lw 3 title "2*(FCI ${atom} + PBE on top)"
plot "data_${atom}2_avdz_cas66_truncated" u 1:2 w l lw 3 title "CAS", "" u 1:(\$2+\$5) w l lw 3 title "CAS+PBE on top", "data_${atom}2_avdz_cas66" u 1:3 w l lw 3 title "FCI", "" u 1:(\$3+\$8) w l lw 3 title "FCI+PBE on top"
set term eps 
set output "${atom}2_${basis}_PBE_on_top_tot.eps"
replot


#plot "data_${atom}2_avdz_cas66_truncated" u 1:2 w l lw 3 title "CAS", -108.7797414 w l lw 3 title "2*${atom} CAS", "" u 1:(\$2+\$6) w l lw 3 title "CAS+PBE on top", -108.89009886 w l lw 3 title "2*(CAS ${atom} + PBE on top)", "data_${atom}2_avdz_cas66" u 1:3 w l lw 3 title "FCI", -108.9739012 w l lw 3 title "2*${atom} FCI", "" u 1:(\$3+\$9) w l lw 3 title "FCI+PBE on top", -109.02120186 w l lw 3 title "2*(FCI ${atom} + PBE on top)"
plot "data_${atom}2_avdz_cas66_truncated" u 1:2 w l lw 3 title "CAS", "" u 1:(\$2+\$6) w l lw 3 title "CAS+PBE on top",  "data_${atom}2_avdz_cas66" u 1:3 w l lw 3 title "FCI",  "" u 1:(\$3+\$9) w l lw 3 title "FCI+PBE on top"
set term eps 
set output "${atom}2_${basis}_PBE_on_top_den_tot.eps"
replot

EOF

gnuplot plot_truncated.plt
