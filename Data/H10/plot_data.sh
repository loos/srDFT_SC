#!/bin/bash

basis=tz
atom=H
echo data_${atom}10_v${basis}_cas1010

 # DZ 
 FCI=-4.992784034175441
 FCI_LDA=-4.992784034175441
 FCI_PBE=-4.992784034175441
 FCI_PBEont=-4.992784034175441
 FCI_PBEontden=-4.992784034175441
## TZ 
 FCI=-4.998098113018433
 FCI_LDA=-4.998098113018433
 FCI_PBE=-4.998098113018433
 FCI_PBEont=-4.998098113018433
 FCI_PBEontden=-4.998098113018433

 exact=-5.00000


cat <<EOF > plot.plt
plot "data_${atom}10_v${basis}_cas1010" u 1:2 w l lw 3 title "FCI ${basis}", $FCI  w l lw 3 title "2*${atom} FCI", "" u 1:(\$2+\$6) w l lw 3 title "FCI ${basis}+LDA", $FCI_LDA   w l lw 3 title "2*(FCI ${atom} + LDA)", 'exact-${atom}10' u (\$1 ):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}2_${basis}_LDA.eps"
replot

plot "data_${atom}10_v${basis}_cas1010" u 1:2 w l lw 3 title "FCI ${basis}", $FCI w l lw 3 title "2*${atom} FCI", "" u 1:(\$2+\$7) w l lw 3 title "FCI ${basis}+PBE", $FCI_PBE  w l lw 3 title "2*(FCI ${atom} + PBE)", 'exact-${atom}10' u (\$1 ):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}2_${basis}_PBE.eps"
replot

plot "data_${atom}10_v${basis}_cas1010" u 1:2 w l lw 3 title "FCI ${basis}", $FCI w l lw 3 title "2*${atom} FCI", "" u 1:(\$2+\$8) w l lw 3 title "FCI ${basis}+PBE on top", $FCI_PBEont w l lw 3 title "2*(FCI ${atom} + PBE on top)", 'exact-${atom}10' u (\$1 ):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}2_${basis}_PBE_on_top.eps"
replot


plot "data_${atom}10_v${basis}_cas1010" u 1:2 w l lw 3 title "FCI ${basis}", $FCI w l lw 3 title "2*${atom} FCI", "" u 1:(\$2+\$9) w l lw 3 title "FCI ${basis}+PBE on top / density", $FCI_PBEontden  w l lw 3 title "2*(FCI ${atom} + PBE on top / density)", 'exact-${atom}10' u (\$1 ):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}2_${basis}_PBE_on_top_den.eps"
replot

set yrange [:0.05]
plot "data_${atom}10_v${basis}_cas1010" u 1:(\$2 - $FCI) w l lw 3 title "FCI ${basis}", "" u 1:(\$2+\$6 -$FCI_PBEontden ) w l lw 3 title "FCI ${basis}+PBE on top / density" , 'exact-${atom}10' u (\$1 ):(\$2 - $exact) w l lw 3 smooth csplines title "Exact spline", 'exact-${atom}10' u (\$1 ):(\$2 - $exact) w p ps 0.5  notitle 
set term eps 
set output "${atom}2_${basis}_PBE_on_top_den_vs_exact.eps"
replot

EOF

gnuplot plot.plt
