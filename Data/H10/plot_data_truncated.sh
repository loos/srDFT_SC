#!/bin/bash

basis=dz
atom=H
echo data_${atom}10_v${basis}_cas1010_truncated

ls data_${atom}10_v${basis}_cas1010_truncated

 CAS=-108.77974145822795
 CAS_LDA=-108.91410109542795
 CAS_PBE=-108.92104742302794
 CAS_PBEot=-108.90744344182795
 CAS_PBEotden=-108.88995440442794

#CAS=-108.79521904536888
#CAS_LDA=-108.92959598016888
#CAS_PBE=-108.93664255376888
#CAS_PBEot=-108.92304394976888
#CAS_PBEotden=-108.90563120596889


 exact=-109.1698

cat <<EOF > plot_truncated.plt
plot "data_${atom}10_v${basis}_cas1010_truncated" u 1:2 w l lw 3 title "CAS", $CAS  w l lw 3 title "2*${atom} CAS", "" u 1:(\$2+\$3) w l lw 3 title "CAS+LDA", $CAS_LDA  w l lw 3 title "2*(CAS ${atom} + LDA)", 'exact-${atom}10' u (\$1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}10_${basis}_LDA_truncated.eps"
replot

plot "data_${atom}10_v${basis}_cas1010_truncated" u 1:2 w l lw 3 title "CAS", $CAS w l lw 3 title "2*${atom} CAS", "" u 1:(\$2+\$4) w l lw 3 title "CAS+PBE", $CAS_PBE  w l lw 3 title "2*(CAS ${atom} + PBE)",  'exact-${atom}10' u (\$1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}10_${basis}_PBE_truncated.eps"
replot

plot "data_${atom}10_v${basis}_cas1010_truncated" u 1:2 w l lw 3 title "CAS",  $CAS w l lw 3 title "2*${atom} CAS", "" u 1:(\$2+\$5) w l lw 3 title "CAS+PBE on top", $CAS_PBEot w l lw 3 title "2*(CAS ${atom} + PBE on top)",  'exact-${atom}10' u (\$1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}10_${basis}_PBE_on_top_truncated.eps"
replot


plot "data_${atom}10_v${basis}_cas1010_truncated" u 1:2 w l lw 3 title "CAS",  $CAS w l lw 3 title "2*${atom} CAS", "" u 1:(\$2+\$6) w l lw 3 title "CAS+PBE on top", $CAS_PBEotden  w l lw 3 title "2*(CAS ${atom} + PBE on top)",  'exact-${atom}10' u (\$1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}10_${basis}_PBE_on_top_den_truncated.eps"
replot

set yrange [:0.1]
plot "data_${atom}10_v${basis}_cas1010_truncated" u 1:(\$2 - $CAS) w l lw 3 title "CAS $basis", "" u 1:(\$2+\$6 -$CAS_PBEotden ) w l lw 3 title "CAS $basis +PBE on top / density" , 'exact-${atom}10' u (\$1 * 0.529177):(\$2 - $exact) w l lw 3 smooth csplines title "Exact spline", 'exact-${atom}10' u (\$1 * 0.529177):(\$2 - $exact) w p ps 0.5  notitle 
set term eps 
set output "${atom}10_${basis}_PBE_on_top_den_vs_exact_truncated.eps"
replot



EOF

gnuplot plot_truncated.plt
