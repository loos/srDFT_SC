plot "data_N2_avdz_cas66_truncated" u 1:2 w l lw 3 title "CAS", -108.77974145822795  w l lw 3 title "2*N CAS", "" u 1:($2+$3) w l lw 3 title "CAS+LDA", -108.91410109542795  w l lw 3 title "2*(CAS N + LDA)", 'exact-N2' u ($1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "N2_dz_LDA_truncated.eps"
replot

plot "data_N2_avdz_cas66_truncated" u 1:2 w l lw 3 title "CAS", -108.77974145822795 w l lw 3 title "2*N CAS", "" u 1:($2+$4) w l lw 3 title "CAS+PBE", -108.92104742302794  w l lw 3 title "2*(CAS N + PBE)",  'exact-N2' u ($1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "N2_dz_PBE_truncated.eps"
replot

plot "data_N2_avdz_cas66_truncated" u 1:2 w l lw 3 title "CAS",  -108.77974145822795 w l lw 3 title "2*N CAS", "" u 1:($2+$5) w l lw 3 title "CAS+PBE on top", -108.90744344182795 w l lw 3 title "2*(CAS N + PBE on top)",  'exact-N2' u ($1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "N2_dz_PBE_on_top_truncated.eps"
replot


plot "data_N2_avdz_cas66_truncated" u 1:2 w l lw 3 title "CAS",  -108.77974145822795 w l lw 3 title "2*N CAS", "" u 1:($2+$6) w l lw 3 title "CAS+PBE on top", -108.88995440442794  w l lw 3 title "2*(CAS N + PBE on top)",  'exact-N2' u ($1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "N2_dz_PBE_on_top_den_truncated.eps"
replot

set yrange [:0.1]
plot "data_N2_avdz_cas66_truncated" u 1:($2 - -108.77974145822795) w l lw 3 title "CAS dz", "" u 1:($2+$6 --108.88995440442794 ) w l lw 3 title "CAS dz +PBE on top / density" , 'exact-N2' u ($1 * 0.529177):($2 - -109.1698) w l lw 3 smooth csplines title "Exact spline", 'exact-N2' u ($1 * 0.529177):($2 - -109.1698) w p ps 0.5  notitle 
set term eps 
set output "N2_dz_PBE_on_top_den_vs_exact_truncated.eps"
replot



