plot "data_N2_avdz_cas66" u 1:3 w l lw 3 title "FCI dz", -108.9739012  w l lw 3 title "2*N FCI", "" u 1:($3+$6) w l lw 3 title "FCI dz+LDA", -109.02698261   w l lw 3 title "2*(FCI N + LDA)", 'exact-N2' u ($1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "N2_dz_LDA.eps"
replot

plot "data_N2_avdz_cas66" u 1:3 w l lw 3 title "FCI dz", -108.9739012 w l lw 3 title "2*N FCI", "" u 1:($3+$7) w l lw 3 title "FCI dz+PBE", -109.02004929  w l lw 3 title "2*(FCI N + PBE)", 'exact-N2' u ($1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "N2_dz_PBE.eps"
replot

plot "data_N2_avdz_cas66" u 1:3 w l lw 3 title "FCI dz", -108.9739012 w l lw 3 title "2*N FCI", "" u 1:($3+$8) w l lw 3 title "FCI dz+PBE on top", -109.02337969 w l lw 3 title "2*(FCI N + PBE on top)", 'exact-N2' u ($1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "N2_dz_PBE_on_top.eps"
replot


plot "data_N2_avdz_cas66" u 1:3 w l lw 3 title "FCI dz", -108.9739012 w l lw 3 title "2*N FCI", "" u 1:($3+$9) w l lw 3 title "FCI dz+PBE on top / density", 'exact-N2' u ($1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "N2_dz_PBE_on_top_den.eps"
replot

plot "data_N2_avdz_cas66" u 1:3 w l lw 3 title "FCI dz", -108.9739012 w l lw 3 title "2*N FCI", "" u 1:($3+$10) w l lw 3 title "FCI dz+LYP on top / density", 'exact-N2' u ($1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "N2_dz_LYP_on_top_den.eps"
replot


set yrange [:0.1]
plot "data_N2_avdz_cas66" u 1:($3 - -108.9739012) w l lw 3 title "FCI dz", "" u 1:($3+$9 --109.02120186 ) w l lw 3 title "FCI dz+PBE on top / density" , 'exact-N2' u ($1 * 0.529177):($2 - -109.1698) w l lw 3 smooth csplines title "Exact spline", 'exact-N2' u ($1 * 0.529177):($2 - -109.1698) w p ps 0.5  notitle 
set term eps 
set output "N2_dz_PBE_on_top_den_vs_exact.eps"
replot

