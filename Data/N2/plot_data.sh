#!/bin/bash

basis=dz
atom=N
echo data_${atom}2_av${basis}_cas66

 FCI=-108.9739012
 FCI_LDA=-109.02698261
 FCI_PBE=-109.02004929
 FCI_PBEont=-109.02337969
 FCI_PBEontden=-109.02120186

#FCI=-109.03390852
#FCI_LDA=-109.05776664519999
#FCI_PBE=-109.0534328248
#FCI_PBEont=-109.05786915259999
#FCI_PBEontden=-109.057425162

 exact=-109.1698


cat <<EOF > plot.plt
plot "data_${atom}2_av${basis}_cas66" u 1:3 w l lw 3 title "FCI ${basis}", $FCI  w l lw 3 title "2*${atom} FCI", "" u 1:(\$3+\$6) w l lw 3 title "FCI ${basis}+LDA", $FCI_LDA   w l lw 3 title "2*(FCI ${atom} + LDA)", 'exact-${atom}2' u (\$1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}2_${basis}_LDA.eps"
replot

plot "data_${atom}2_av${basis}_cas66" u 1:3 w l lw 3 title "FCI ${basis}", $FCI w l lw 3 title "2*${atom} FCI", "" u 1:(\$3+\$7) w l lw 3 title "FCI ${basis}+PBE", $FCI_PBE  w l lw 3 title "2*(FCI ${atom} + PBE)", 'exact-${atom}2' u (\$1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}2_${basis}_PBE.eps"
replot

plot "data_${atom}2_av${basis}_cas66" u 1:3 w l lw 3 title "FCI ${basis}", $FCI w l lw 3 title "2*${atom} FCI", "" u 1:(\$3+\$8) w l lw 3 title "FCI ${basis}+PBE on top", $FCI_PBEont w l lw 3 title "2*(FCI ${atom} + PBE on top)", 'exact-${atom}2' u (\$1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}2_${basis}_PBE_on_top.eps"
replot


plot "data_${atom}2_av${basis}_cas66" u 1:3 w l lw 3 title "FCI ${basis}", $FCI w l lw 3 title "2*${atom} FCI", "" u 1:(\$3+\$9) w l lw 3 title "FCI ${basis}+PBE on top / density", 'exact-${atom}2' u (\$1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}2_${basis}_PBE_on_top_den.eps"
replot

plot "data_${atom}2_av${basis}_cas66" u 1:3 w l lw 3 title "FCI ${basis}", $FCI w l lw 3 title "2*${atom} FCI", "" u 1:(\$3+\$10) w l lw 3 title "FCI ${basis}+LYP on top / density", 'exact-${atom}2' u (\$1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}2_${basis}_LYP_on_top_den.eps"
replot


set yrange [:0.1]
plot "data_${atom}2_av${basis}_cas66" u 1:(\$3 - $FCI) w l lw 3 title "FCI ${basis}", "" u 1:(\$3+\$9 -$FCI_PBEontden ) w l lw 3 title "FCI ${basis}+PBE on top / density" , 'exact-${atom}2' u (\$1 * 0.529177):(\$2 - $exact) w l lw 3 smooth csplines title "Exact spline", 'exact-${atom}2' u (\$1 * 0.529177):(\$2 - $exact) w p ps 0.5  notitle 
set term eps 
set output "${atom}2_${basis}_PBE_on_top_den_vs_exact.eps"
replot

EOF

gnuplot plot.plt
