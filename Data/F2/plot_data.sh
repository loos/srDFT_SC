#!/bin/bash

basis=tz
atom=F
echo data_${atom}2_av${basis}_cas22

 FCI=-199.1005769
 FCI_LDA=-199.2308582 
 FCI_PBE=-199.2529071 
 FCI_PBEont=-199.2516433 
 FCI_PBEontden=-199.24250070

 FCI=-199.25614876
 FCI_LDA=-199.3214776
 FCI_PBE=-199.32328160
 FCI_PBEont=-199.3300198
 FCI_PBEontden=-199.32825102

 exact=-199.4448


cat <<EOF > plot.plt
plot "data_${atom}2_av${basis}_cas22" u 1:3 w l lw 3 title "FCI ${basis}", $FCI  w l lw 3 title "2*${atom} FCI", "" u 1:(\$3+\$6) w l lw 3 title "FCI ${basis}+LDA", $FCI_LDA   w l lw 3 title "2*(FCI ${atom} + LDA)", 'exact-${atom}2' u (\$1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}2_${basis}_LDA.eps"
replot

plot "data_${atom}2_av${basis}_cas22" u 1:3 w l lw 3 title "FCI ${basis}", $FCI w l lw 3 title "2*${atom} FCI", "" u 1:(\$3+\$7) w l lw 3 title "FCI ${basis}+PBE", $FCI_PBE  w l lw 3 title "2*(FCI ${atom} + PBE)", 'exact-${atom}2' u (\$1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}2_${basis}_PBE.eps"
replot

plot "data_${atom}2_av${basis}_cas22" u 1:3 w l lw 3 title "FCI ${basis}", $FCI w l lw 3 title "2*${atom} FCI", "" u 1:(\$3+\$8) w l lw 3 title "FCI ${basis}+PBE on top", $FCI_PBEont w l lw 3 title "2*(FCI ${atom} + PBE on top)", 'exact-${atom}2' u (\$1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}2_${basis}_PBE_on_top.eps"
replot


plot "data_${atom}2_av${basis}_cas22" u 1:3 w l lw 3 title "FCI ${basis}", $FCI w l lw 3 title "2*${atom} FCI", "" u 1:(\$3+\$9) w l lw 3 title "FCI ${basis}+PBE on top / density", $FCI_PBEontden  w l lw 3 title "2*(FCI ${atom} + PBE on top / density)", 'exact-${atom}2' u (\$1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "${atom}2_${basis}_PBE_on_top_den.eps"
replot

set yrange [:0.05]
plot "data_${atom}2_av${basis}_cas22" u 1:(\$3 - $FCI) w l lw 3 title "FCI ${basis}", "" u 1:(\$3+\$9 -$FCI_PBEontden ) w l lw 3 title "FCI ${basis}+PBE on top / density" , 'exact-${atom}2' u (\$1 * 0.529177):(\$2 - $exact) w l lw 3 smooth csplines title "Exact spline", 'exact-${atom}2' u (\$1 * 0.529177):(\$2 - $exact) w p ps 0.5  notitle 
set term eps 
set output "${atom}2_${basis}_PBE_on_top_den_vs_exact.eps"
replot

EOF

gnuplot plot.plt
