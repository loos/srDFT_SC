plot "data_F2_avtz_cas22" u 1:3 w l lw 3 title "FCI tz", -199.25614876  w l lw 3 title "2*F FCI", "" u 1:($3+$6) w l lw 3 title "FCI tz+LDA", -199.3214776   w l lw 3 title "2*(FCI F + LDA)", 'exact-F2' u ($1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "F2_tz_LDA.eps"
replot

plot "data_F2_avtz_cas22" u 1:3 w l lw 3 title "FCI tz", -199.25614876 w l lw 3 title "2*F FCI", "" u 1:($3+$7) w l lw 3 title "FCI tz+PBE", -199.32328160  w l lw 3 title "2*(FCI F + PBE)", 'exact-F2' u ($1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "F2_tz_PBE.eps"
replot

plot "data_F2_avtz_cas22" u 1:3 w l lw 3 title "FCI tz", -199.25614876 w l lw 3 title "2*F FCI", "" u 1:($3+$8) w l lw 3 title "FCI tz+PBE on top", -199.3300198 w l lw 3 title "2*(FCI F + PBE on top)", 'exact-F2' u ($1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "F2_tz_PBE_on_top.eps"
replot


plot "data_F2_avtz_cas22" u 1:3 w l lw 3 title "FCI tz", -199.25614876 w l lw 3 title "2*F FCI", "" u 1:($3+$9) w l lw 3 title "FCI tz+PBE on top / density", -199.32825102  w l lw 3 title "2*(FCI F + PBE on top / density)", 'exact-F2' u ($1 * 0.529177):2 w l lw 3 smooth csplines title "Exact spline"
set term eps 
set output "F2_tz_PBE_on_top_den.eps"
replot

set yrange [:0.05]
plot "data_F2_avtz_cas22" u 1:($3 - -199.25614876) w l lw 3 title "FCI tz", "" u 1:($3+$9 --199.32825102 ) w l lw 3 title "FCI tz+PBE on top / density" , 'exact-F2' u ($1 * 0.529177):($2 - -199.4448) w l lw 3 smooth csplines title "Exact spline", 'exact-F2' u ($1 * 0.529177):($2 - -199.4448) w p ps 0.5  notitle 
set term eps 
set output "F2_tz_PBE_on_top_den_vs_exact.eps"
replot

