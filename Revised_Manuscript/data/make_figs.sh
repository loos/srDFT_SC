#!/bin/bash


for i in H10
do
  for j in vdz vtz vqz
  do
    pushd ../../new/${i}_$j/plots
    ./plot.sh
    cp *relat*pdf ../../../Manuscript/data/$i
    popd
  done
done
  


for i in N2 F2 O2
do
  for j in avdz avtz
  do
    pushd ../../new/${i}_$j/plots
    ./plot.sh
    cp *relat*pdf ../../../Manuscript/data/$i
    popd
  done
done
  

