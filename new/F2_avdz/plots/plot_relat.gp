set xrange [2.5:3.5]
set key left 

set xlabel "Internuclear distance (bohr) "
set ylabel "Atomization energy (hartree)"
set key font ",15"
set key spacing "1,8"

plot   'E_relat_dft' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1  title "FCI/avdz"
replot 'E_relat_dft' using 1:3 smooth cspline notitle lt 9 , "" using 1:3 w p lt 9 ps 1  title "FCI+PBE-UEG/avdz" 
replot 'E_relat_dft' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4 ps 1  title "FCI+PBE-OT/avdz" 
replot 'E_relat_dft' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1  title "FCI+SU-PBE-OT/avdz" 
replot 'E_relat_exact' using 1:2 smooth cspline notitle lt 7 , "" using 1:2 w p lt 7 ps 1  title "Exact" 
set terminal eps enhanced linewidth 4
set output "DFT_avdzE_relat_zoom.eps"
replot

