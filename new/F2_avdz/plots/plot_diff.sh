
WF=FCI
BASIS=avdz
BASIS2=aug-cc-pVDZ
METHOD=DFT

TYPE=relat
FILE=data_${METHOD}_${BASIS}E_${TYPE}
OUT=${METHOD}_${BASIS}E_${TYPE}
#lt -1
cat << EOF > pouet.gp
set xrange [:5]
set key bottom

set xlabel "Internuclear distance (bohr) "
set ylabel "Atomization energy (hartree)"
set key font ",15"
set key spacing "1,8"

set term pdf
set output "${OUT}.pdf"
plot   '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1 title "${WF}/$BASIS2"
replot '${FILE}' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1 title "${WF}+PBEot0{/Symbol z}/$BASIS2" 
replot '${FILE}' using 1:6 smooth cspline notitle lt 7 , "" using 1:6 w p lt 7 ps 1 title "Exact" 

EOF

gnuplot pouet.gp
if [[ $METHOD == "DFT" ]]; then

OUT=${METHOD}_${BASIS}E_${TYPE}_zoom
cat << EOF > pouet.gp
set xrange [2.5:3.5]
set key left 

set xlabel "Internuclear distance (bohr) "
set ylabel "Atomization energy (hartree)"
set key font ",15"
set key spacing "1,8"

set term pdf
set output "${OUT}.pdf"
plot   '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1  title "${WF}/$BASIS"
replot '${FILE}' using 1:3 smooth cspline notitle lt 9 , "" using 1:3 w p lt 9 ps 1  title "${WF}+PBE-UEG~{/Symbol z}{.8-}/$BASIS" 
replot '${FILE}' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4 ps 1  title "${WF}+PBEot~{/Symbol z}{.8-}/$BASIS" 
replot '${FILE}' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1  title "${WF}+PBEot0{/Symbol z}/$BASIS" 
replot '${FILE}' using 1:6 smooth cspline notitle lt 7 , "" using 1:6 w p lt 7 ps 1  title "Exact" 

EOF

fi

gnuplot pouet.gp

TYPE=error
FILE=data_${METHOD}_${BASIS}E_${TYPE}
OUT=${METHOD}_${BASIS}E_${TYPE}
#lt -1
cat << EOF > pouet.gp
set xrange [:7]
set xlabel "Internuclear distance (bohr) "
set ylabel "Atomization energy (hartree)"
set key font ",15"
set key spacing "1,8"
plot   '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2  ps 1  title "${WF}/$BASIS"
replot '${FILE}' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8  ps 1  title "${WF}+PBEot{/Symbol z}/$BASIS" 
replot '${FILE}' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4  ps 1  title "${WF}+PBEot/$BASIS" 
replot '${FILE}' using 1:3 smooth cspline notitle lt 9 , "" using 1:3 w p lt 9  ps 1  title "${WF}+PBE/$BASIS" 
set terminal eps enhanced linewidth 6
set output "${OUT}.eps"
replot

EOF

gnuplot pouet.gp

