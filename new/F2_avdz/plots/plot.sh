#!/bin/bash

WF=FCI
BASIS=avdz
BASIS2=aug-cc-pVDZ
METHOD=DFT

TYPE=relat
FILE=E_relat_dft
FILE2=E_relat_exact
OUT=${METHOD}_${BASIS}E_${TYPE}
#lt -1
cat << EOF > pouet.gp
set xrange [:5]
set key bottom
set format y "%.2f"
set format x "%.1f"
set grid
set style textbox opaque noborder
set style line 12 lc rgb '#dddddd' lt 1 lw 0.5
set grid xtics mxtics ytics mytics back ls 12, ls 12
#set label 1 at screen 0.01, 0.03 front "a) F_{2}, $BASIS2"  

set xlabel "Internuclear distance (bohr)"
set ylabel "Atomization energy (hartree)"

plot   '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1 title "${WF}"
replot '${FILE}' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1 title "${WF}+SU-PBE-OT" 
replot '${FILE2}' using 1:2 smooth cspline notitle lt 7 , "" using 1:2 w p lt 7 ps 1 title "Exact" 
replot   '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1 notitle 
replot '${FILE}' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1 notitle 

set terminal pdf enhanced\
  font "Times,16" \
   solid dashlength 1.0 linewidth 2. rounded \
   size 4.0in, 3.0in 
set output "${OUT}.pdf"
replot


EOF

gnuplot pouet.gp
if [[ $METHOD == "DFT" ]]; then

OUT=${METHOD}_${BASIS}E_${TYPE}_zoom
cat << EOF > pouet_relat.gp
set xrange [2.5:3.5]
set key at 3.1, -0.022
set format y "%.3f"
set format x "%.1f"
set grid
set style textbox opaque noborder
set style line 12 lc rgb '#dddddd' lt 1 lw 0.5
set grid xtics mxtics ytics mytics back ls 12, ls 12
#set label 1 at screen 0.01, 0.03 front "b) F_{2}, $BASIS2"  

set xlabel "Internuclear distance (bohr)"
set ylabel "Atomization energy (hartree)"

plot '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1  title "${WF}"
replot '${FILE}' using 1:3 smooth cspline notitle lt 9 , "" using 1:3 w p lt 9 ps 1  title "${WF}+PBE-UEG"
replot '${FILE}' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4 ps 1  title "${WF}+PBE-OT"
replot '${FILE}' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1  title "${WF}+SU-PBE-OT" 
replot '${FILE2}' using 1:2 smooth cspline notitle lt 7 , "" using 1:2 w p lt 7 ps 1  title "Exact" 
replot '${FILE}' using 1:3 smooth cspline notitle lt 9 , "" using 1:3 w p lt 9 ps 1  notitle 
replot '${FILE}' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4 ps 1  notitle 
replot '${FILE}' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1  notitle 

set terminal pdf enhanced\
  font "Times,16" \
   solid dashlength 1.0 linewidth 2. rounded \
   size 4.0in, 3.0in 
set output "${OUT}.pdf"
replot


EOF

fi

gnuplot pouet_relat.gp
exit 

TYPE=error
FILE=data_${METHOD}_${BASIS}E_${TYPE}
OUT=${METHOD}_${BASIS}E_${TYPE}
#lt -1
cat << EOF > pouet.gp
set xrange [:7]
set xlabel "Internuclear distance (bohr)"
set ylabel "Atomization energy (hartree)"
set format y "%.3f"
set format x "%.1f"
set grid
set title "F_{2}, $BASIS2"
plot   '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2  ps 1  title "${WF}"
replot '${FILE}' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8  ps 1  title "${WF}+PBEot{/Symbol z}" 
replot '${FILE}' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4  ps 1  title "${WF}+PBEot" 
replot '${FILE}' using 1:3 smooth cspline notitle lt 9 , "" using 1:3 w p lt 9  ps 1  title "${WF}+PBE" 

set terminal pdf enhanced\
  font "Times" \
   solid dashlength 1.0 linewidth 2. rounded \
   size 4.0in, 3.0in 
set output "${OUT}.pdf"
replot


EOF

gnuplot pouet.gp

