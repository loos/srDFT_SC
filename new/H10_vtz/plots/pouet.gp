set xrange [1.5:2.4]
set yrange [:-0.495]
set key top center
set format y "%.2f"
set format x "%.1f"
set grid
set style textbox opaque noborder
set style line 12 lc rgb '#dddddd' lt 1 lw 0.5
set grid xtics mxtics ytics mytics back ls 12, ls 12
#set label 1 at screen 0.01, 0.03 front "d) H_{10}, cc-pVTZ" 

set xlabel "Internuclear distance (bohr)"
set ylabel "Atomization energy (hartree)"


plot   'data_DFT_vtzE_relat' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1  title "MRCI+Q"
replot 'data_DFT_vtzE_relat' using 1:3 smooth cspline notitle lt 9 , "" using 1:3 w p lt 9 ps 1  title "(MRCI+Q)+PBE-UEG"
replot 'data_DFT_vtzE_relat' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4 ps 1  title "(MRCI+Q)+PBE-OT"
replot 'data_DFT_vtzE_relat' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1  title "(MRCI+Q)+SU-PBE-OT"
replot 'data_DFT_vtzE_relat' using 1:6 smooth cspline notitle lt 7 , "" using 1:6 w p lt 7 ps 1  title "Exact" 
replot 'data_DFT_vtzE_relat' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1  notitle
replot 'data_DFT_vtzE_relat' using 1:3 smooth cspline notitle lt 9 , "" using 1:3 w p lt 9 ps 1  notitle
replot 'data_DFT_vtzE_relat' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4 ps 1  notitle
replot 'data_DFT_vtzE_relat' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1  notitle

set terminal pdf enhanced  font "Times,16"    solid dashlength 1.0 linewidth 2. rounded    size 4.0in, 3.0in 
set output "DFT_vtzE_relat_zoom.pdf"
replot

set terminal eps linewidth 6
set output "DFT_vtzE_relat_zoom.eps"
replot

