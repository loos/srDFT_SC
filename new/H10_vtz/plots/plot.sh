#!/bin/bash

WF=FCI
BASIS=vtz
BASIS2=cc-pVTZ
METHOD=DFT

TYPE=relat
FILE=data_${METHOD}_${BASIS}E_${TYPE}
OUT=${METHOD}_${BASIS}E_${TYPE}
#lt -1
cat << EOF > pouet.gp
set yrange [:0]
set xrange [:3.6]
set key at 3.1, -0.05
set format y "%.2f"
set format x "%.1f"
set grid
set style textbox opaque noborder
set style line 12 lc rgb '#dddddd' lt 1 lw 0.5
set grid xtics mxtics ytics mytics back ls 12, ls 12
#set label 1 at screen 0.01, 0.03 front "c) H_{10}, $BASIS2" 

set xlabel "Internuclear distance (bohr)"
set ylabel "Atomization energy (hartree)"

plot   '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1 title "(MRCI+Q)"
replot '${FILE}' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1 title "(MRCI+Q)+SU-PBE-OT"
replot '${FILE}' using 1:6 smooth cspline notitle lt 7 , "" using 1:6 w p lt 7 ps 1 title "Exact" 
replot '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1 notitle 
replot '${FILE}' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1 notitle 

set terminal pdf enhanced\
  font "Times,16" \
   solid dashlength 1.0 linewidth 2. rounded \
   size 4.0in, 3.0in 
set output "${OUT}.pdf"
replot


EOF

gnuplot pouet.gp
if [[ $METHOD == "DFT" ]]; then

OUT=${METHOD}_${BASIS}E_${TYPE}_zoom
cat << EOF > pouet.gp
set xrange [1.5:2.4]
set yrange [:-0.495]
set key top center
set format y "%.2f"
set format x "%.1f"
set grid
set style textbox opaque noborder
set style line 12 lc rgb '#dddddd' lt 1 lw 0.5
set grid xtics mxtics ytics mytics back ls 12, ls 12
#set label 1 at screen 0.01, 0.03 front "d) H_{10}, $BASIS2" 

set xlabel "Internuclear distance (bohr)"
set ylabel "Atomization energy (hartree)"


plot   '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1  title "MRCI+Q"
replot '${FILE}' using 1:3 smooth cspline notitle lt 9 , "" using 1:3 w p lt 9 ps 1  title "(MRCI+Q)+PBE-UEG"
replot '${FILE}' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4 ps 1  title "(MRCI+Q)+PBE-OT"
replot '${FILE}' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1  title "(MRCI+Q)+SU-PBE-OT"
replot '${FILE}' using 1:6 smooth cspline notitle lt 7 , "" using 1:6 w p lt 7 ps 1  title "Exact" 
replot '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1  notitle
replot '${FILE}' using 1:3 smooth cspline notitle lt 9 , "" using 1:3 w p lt 9 ps 1  notitle
replot '${FILE}' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4 ps 1  notitle
replot '${FILE}' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1  notitle

set terminal pdf enhanced\
  font "Times,16" \
   solid dashlength 1.0 linewidth 2. rounded \
   size 4.0in, 3.0in 
set output "${OUT}.pdf"
replot

set terminal eps linewidth 6
set output "${OUT}.eps"
replot

EOF

fi

gnuplot pouet.gp
exit 0

TYPE=error
FILE=data_${METHOD}_${BASIS}E_${TYPE}
OUT=${METHOD}_${BASIS}E_${TYPE}
#lt -1
cat << EOF > pouet.gp

set xrange [:3.6]
set xlabel "Internuclear distance (bohr)"
set ylabel "Atomization energy (hartree)"
set format y "%.2f"
set format x "%.1f"
set grid
set title "H_{10}, $BASIS2"

plot   '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2  title "${WF}"
replot '${FILE}' using 1:3 smooth cspline notitle lt 8 , "" using 1:3 w p lt 8  title "${WF}+PBE"
replot '${FILE}' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4  title "${WF}+PBEot"


set terminal pdf enhanced\
  font "Times,16" \
   solid dashlength 1.0 linewidth 2. rounded \
   size 4.0in, 3.0in 
set output "${OUT}.pdf"
replot
EOF

gnuplot pouet.gp

