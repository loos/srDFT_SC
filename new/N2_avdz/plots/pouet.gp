set xrange [:7]
set xlabel "Internuclear distance (bohr) "
set ylabel "Atomization energy (hartree)"
set format y "%.2f"
set format x "%.1f"
set grid
set title "N_{2}, aug-cc-pVDZ"
plot   'data_DFT_avdzE_error' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2  ps 1  title "FCI"
replot 'data_DFT_avdzE_error' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8  ps 1  title "FCI+PBEot{/Symbol z}" 
replot 'data_DFT_avdzE_error' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4  ps 1  title "FCI+PBEot" 
replot 'data_DFT_avdzE_error' using 1:3 smooth cspline notitle lt 9 , "" using 1:3 w p lt 9  ps 1  title "FCI+PBE" 
replot 'data_DFT_avdzE_error' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2  ps 1  notitle 
replot 'data_DFT_avdzE_error' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8  ps 1  notitle
replot 'data_DFT_avdzE_error' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4  ps 1  notitle
replot 'data_DFT_avdzE_error' using 1:3 smooth cspline notitle lt 9 , "" using 1:3 w p lt 9  ps 1  notitle
set terminal pdf enhanced  font "Times,16"    solid dashlength 1.0 linewidth 2. rounded    size 4.0in, 3.0in 
set output "DFT_avdzE_error.pdf"
replot


