set xrange [:5]
set key bottom
set format y "%.2f"
set format x "%.1f"
set style textbox opaque noborder
set style line 12 lc rgb '#dddddd' lt 1 lw 0.5
set grid xtics mxtics ytics mytics back ls 12, ls 12
#set label 1 at screen 0.01, 0.03 front "c) F_{2}, aug-cc-pVTZ"  

set xlabel "Internuclear distance (bohr)"
set ylabel "Atomization energy (hartree)"

plot   'E_relat_dft' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1 title "FCI"
replot 'E_relat_dft' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1 title "FCI+SU-PBE-OT" 
replot 'E_relat_exact' using 1:2 smooth cspline notitle lt 7 , "" using 1:2 w p lt 7 ps 1 title "Exact" 
replot   'E_relat_dft' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1 notitle 
replot 'E_relat_dft' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1 notitle 

set terminal pdf enhanced  font "Times,16"    solid dashlength 1.0 linewidth 2. rounded    size 4.0in, 3.0in 
set output "DFT_avtzE_relat.pdf"
replot


