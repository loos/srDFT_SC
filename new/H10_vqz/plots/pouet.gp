
set xrange [:3.6]
set xlabel "Internuclear distance (bohr)"
set ylabel "Atomization energy (hartree)"
set format y "%.2f"
set format x "%.1f"
set grid
set title "H_{10}, cc-pVQZ"

plot   'data_DFT_vqzE_error' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2  title "FCI"
replot 'data_DFT_vqzE_error' using 1:3 smooth cspline notitle lt 8 , "" using 1:3 w p lt 8  title "FCI+PBE"
replot 'data_DFT_vqzE_error' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4  title "FCI+PBEot"


set terminal pdf enhanced  font "Times,16"    solid dashlength 1.0 linewidth 2. rounded    size 4.0in, 3.0in 
set output "DFT_vqzE_error.pdf"
replot
