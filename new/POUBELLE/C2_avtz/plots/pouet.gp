set xrange [:7]
plot   'data_DFT_avtzE_error' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2  ps 0.5  title "FCI/avtz"
replot 'data_DFT_avtzE_error' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8  ps 0.5  title "FCI+PBEot{/Symbol z}/avtz" 
replot 'data_DFT_avtzE_error' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4  ps 0.5  title "FCI+PBEot/avtz" 
replot 'data_DFT_avtzE_error' using 1:3 smooth cspline notitle lt 9 , "" using 1:3 w p lt 9  ps 0.5  title "FCI+PBE/avtz" 
set terminal eps enhanced linewidth 6
set output "DFT_avtzE_error.eps"
replot

