#!/bin/bash

WF=FCI
BASIS=avdz
METHOD=DFT

TYPE=relat
FILE=data_${METHOD}_${BASIS}E_${TYPE}
OUT=${METHOD}_${BASIS}E_${TYPE}
#lt -1
cat << EOF > pouet.gp
set xrange [:5]
set yrange [-0.25:0]
set format y "%.2f"
set format x "%.1f"

set xlabel "Interatomic distance  (a.u.) "
set ylabel "Energy (a.u.)"
set grid

set key bottom

plot   '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1 title "${WF}/$BASIS"
replot '${FILE}' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1 title "${WF}+PBEot0{/Symbol z}/$BASIS" 
replot '${FILE}' using 1:6 smooth cspline notitle lt 7 , "" using 1:6 w p lt 7 ps 1 title "Exact" 

set terminal pdfcairo \
  font "Times,14" \
   solid dashlength 1.0 linewidth 2. rounded \
   size 4.0in, 3.0in 
set output "${OUT}.pdf"
replot
quit


EOF

gnuplot pouet.gp
exit 0

if [[ $METHOD == "DFT" ]]; then

OUT=${METHOD}_${BASIS}E_${TYPE}_zoom
cat << EOF > pouet.gp
set xrange [2.1 :3.0]
set yrange [-0.235:-0.18]
set key bottom
set format y "%.2f"
set format x "%.1f"


set xlabel "Interatomic distance  (a.u.) "
set ylabel "Energy (Hartree)"
set grid

plot   '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1  title "${WF}/$BASIS"
replot '${FILE}' using 1:3 smooth cspline notitle lt 9 , "" using 1:3 w p lt 9 ps 1  title "${WF}+PBE-UEG~{/Symbol z}{.8-}/$BASIS" 
replot '${FILE}' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4 ps 1  title "${WF}+PBEot~{/Symbol z}{.8-}/$BASIS" 
replot '${FILE}' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1  title "${WF}+PBEot0{/Symbol z}/$BASIS" 
replot '${FILE}' using 1:6 smooth cspline notitle lt 7 , "" using 1:6 w p lt 7 ps 1  title "Exact" 
set terminal pdf enhanced
set output "${OUT}.pdf"
replot

EOF

fi

gnuplot pouet.gp

TYPE=error
FILE=data_${METHOD}_${BASIS}E_${TYPE}
OUT=${METHOD}_${BASIS}E_${TYPE}
#lt -1
cat << EOF > pouet.gp
set xrange [:7]

set xlabel "Interatomic distance  (a.u.) "
set ylabel "Energy (Hartree)"
set grid

plot   '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2  ps 1  title "${WF}/$BASIS"
replot '${FILE}' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8  ps 1  title "${WF}+PBEot{/Symbol z}/$BASIS" 
replot '${FILE}' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4  ps 1  title "${WF}+PBEot/$BASIS" 
replot '${FILE}' using 1:3 smooth cspline notitle lt 9 , "" using 1:3 w p lt 9  ps 1  title "${WF}+PBE/$BASIS" 

set terminal pdf enhanced\
  font "Times,14" \
   solid dashlength 1.0 linewidth 2. rounded \
   size 4.0in, 3.0in 
set output "${OUT}.pdf"
replot
quit

EOF

gnuplot pouet.gp

