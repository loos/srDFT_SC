set xrange [:5]
set yrange [-0.25:0]
set format y "%.2f"
set format x "%.1f"

set xlabel "Interatomic distance  (a.u.) "
set ylabel "Energy (a.u.)"
set grid

set key bottom

plot   'data_DFT_avdzE_relat' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 1 title "FCI/avdz"
replot 'data_DFT_avdzE_relat' using 1:5 smooth cspline notitle lt 8 , "" using 1:5 w p lt 8 ps 1 title "FCI+PBEot0{/Symbol z}/avdz" 
replot 'data_DFT_avdzE_relat' using 1:6 smooth cspline notitle lt 7 , "" using 1:6 w p lt 7 ps 1 title "Exact" 

set terminal pdfcairo   font "Times,14"    solid dashlength 1.0 linewidth 2. rounded    size 4.0in, 3.0in 
set output "DFT_avdzE_relat.pdf"
replot
quit


