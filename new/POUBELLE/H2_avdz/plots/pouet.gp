set xrange [:7]
plot   'data_DFT_avdzE_error' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2  ps 0.5  title "FCI/avdz"
replot 'data_DFT_avdzE_error' using 1:5 smooth cspline notitle lt 3 , "" using 1:5 w p lt 3  ps 0.5  title "FCI+PBEot{/Symbol z}/avdz" 
replot 'data_DFT_avdzE_error' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4  ps 0.5  title "FCI+PBEot/avdz" 
replot 'data_DFT_avdzE_error' using 1:3 smooth cspline notitle lt 8 , "" using 1:3 w p lt 8  ps 0.5  title "FCI+PBE/avdz" 
set terminal eps enhanced linewidth 3
set output "DFT_avdzE_error.eps"
replot

