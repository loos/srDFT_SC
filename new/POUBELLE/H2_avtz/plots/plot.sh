
WF=FCI
BASIS=avtz
METHOD=DFT_full

TYPE=relat
FILE=data_${METHOD}_${BASIS}E_${TYPE}
OUT=${METHOD}_${BASIS}E_${TYPE}
#lt -1
cat << EOF > pouet.gp
set xrange [:7]
set yrange [-0.178:0]
set key bottom
plot   '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 0.5 title "${WF}/$BASIS"
replot '${FILE}' using 1:5 smooth cspline notitle lt 3 , "" using 1:5 w p lt 3 ps 0.5 title "${WF}+PBEot0{/Symbol z}/$BASIS" 
replot '${FILE}' using 1:6 smooth cspline notitle lt 7 , "" using 1:6 w p lt 7 ps 0.5 title "Exact" 
set terminal eps enhanced linewidth 3
set output "${OUT}.eps"
replot

EOF

gnuplot pouet.gp

if [[ $METHOD == "DFT" ]]; then

OUT=${METHOD}_${BASIS}E_${TYPE}_zoom
cat << EOF > pouet.gp
set xrange [.7:2.]
set yrange [-0.178:-0.15]
set key bottom left 
plot   '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2 ps 0.5  title "${WF}/$BASIS"
replot '${FILE}' using 1:3 smooth cspline notitle lt 8 , "" using 1:3 w p lt 8 ps 0.5  title "${WF}+PBE/$BASIS" 
replot '${FILE}' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4 ps 0.5  title "${WF}+PBEot/$BASIS" 
replot '${FILE}' using 1:5 smooth cspline notitle lt 3 , "" using 1:5 w p lt 3 ps 0.5  title "${WF}+PBEot0{/Symbol z}/$BASIS" 
replot '${FILE}' using 1:6 smooth cspline notitle lt 7 , "" using 1:6 w p lt 7 ps 0.5  title "Exact" 
set terminal eps enhanced linewidth 3
set output "${OUT}.eps"
replot

EOF

fi
exit 0
gnuplot pouet.gp

TYPE=error
FILE=data_${METHOD}_${BASIS}E_${TYPE}
OUT=${METHOD}_${BASIS}E_${TYPE}
#lt -1
cat << EOF > pouet.gp
set xrange [:7]
plot   '${FILE}' using 1:2 smooth cspline notitle lt 2 , "" using 1:2 w p lt 2  ps 0.5  title "${WF}/$BASIS"
replot '${FILE}' using 1:5 smooth cspline notitle lt 3 , "" using 1:5 w p lt 3  ps 0.5  title "${WF}+PBEot{/Symbol z}/$BASIS" 
replot '${FILE}' using 1:4 smooth cspline notitle lt 4 , "" using 1:4 w p lt 4  ps 0.5  title "${WF}+PBEot/$BASIS" 
replot '${FILE}' using 1:3 smooth cspline notitle lt 8 , "" using 1:3 w p lt 8  ps 0.5  title "${WF}+PBE/$BASIS" 
set terminal eps enhanced linewidth 3
set output "${OUT}.eps"
replot

EOF

gnuplot pouet.gp

