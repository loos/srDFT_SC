set xrange [.7:2.]
set yrange [-0.178:-0.15]
set key bottom
plot 'data_DFT_full_avtzE_relat' using 1:2 smooth cspline notitle lt 4 , "" using 1:2 w p lt 4 ps 0.5 title "FCI/avtz" 
replot 'data_DFT_full_avtzE_relat' using 1:5 smooth cspline notitle lt 2 , "" using 1:5 w p lt 2 ps 0.5 title "FCI+PBEot0{/Symbol z}-full/avtz" 
replot 'data_DFT_avtzE_relat' using 1:5 smooth cspline notitle lt 3 , "" using 1:5 w p lt 3 ps 0.5 title "FCI+PBEot0{/Symbol z}/avtz" 
replot 'data_DFT_full_avtzE_relat' using 1:6 smooth cspline notitle lt 7 , "" using 1:6 w p lt 7 ps 0.5 title "Exact" 
set terminal eps enhanced linewidth 3
set output "DFT_full_avtzE_relat.eps"
replot

